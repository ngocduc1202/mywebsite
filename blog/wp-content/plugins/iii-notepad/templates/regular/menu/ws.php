<?php
/**
 * Created by vagrant.
 * User: vagrant
 * Date: 5/12/2020
 * Time: 10:03 PM
 */

?>

<select>
	<option class="hidden">
		<?php echo esc_html__('Select Action', 'iii-notepad'); ?>
	</option>
	<option>
		<?php echo esc_html__('New', 'iii-notepad'); ?>
	</option>
	<option>
		<?php echo esc_html__('Open', 'iii-notepad'); ?>
	</option>
	<option>
		<?php echo esc_html__('Save', 'iii-notepad'); ?>
	</option>
	<option>
		<?php echo esc_html__('Open Worksheet', 'iii-notepad'); ?>
	</option>
</select>
